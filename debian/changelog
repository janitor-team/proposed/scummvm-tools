scummvm-tools (2.6.0-2) unstable; urgency=medium

  * Switch to wxwidgets3.2. Closes: #1019793.

 -- Stephen Kitt <skitt@debian.org>  Fri, 16 Sep 2022 14:18:00 +0200

scummvm-tools (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 07 Aug 2022 18:02:06 +0200

scummvm-tools (2.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 31 Oct 2021 14:50:04 +0100

scummvm-tools (2.2.0-1) unstable; urgency=medium

  * New upstream release, obsoleting freetype2-pkg-config.patch.
  * Switch to debhelper compatibility level 13.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 02 Oct 2020 09:04:53 +0200

scummvm-tools (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.4.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 18 Oct 2019 18:13:06 +0200

scummvm-tools (2.0.0-5) unstable; urgency=medium

  * Allow cross-building.

 -- Stephen Kitt <skitt@debian.org>  Wed, 11 Sep 2019 22:04:38 +0200

scummvm-tools (2.0.0-4) unstable; urgency=medium

  * Use the Perl test generator instead of the Python 2 version.
    Closes: #938453.

 -- Stephen Kitt <skitt@debian.org>  Fri, 06 Sep 2019 20:57:03 +0200

scummvm-tools (2.0.0-3) unstable; urgency=medium

  [ Reiner Herrmann ]
  * Update watch file.

  [ Stephen Kitt ]
  * Switch to Gtk 3. Closes: #933461.
  * Switch to debhelper compatibility level 12.
  * Update spelling fixes.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.4.0, no further change required.
  * Explicitly build-depend on pkg-config.

 -- Stephen Kitt <skitt@debian.org>  Mon, 05 Aug 2019 20:54:12 +0200

scummvm-tools (2.0.0-2) unstable; urgency=medium

  * Migrate to Salsa.
  * Use pkg-config to find freetype2. Closes: #892443.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 15 May 2018 22:54:17 +0200

scummvm-tools (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper compatibility level 11.
  * Use a secure upstream URL in debian/watch.
  * Add manpages for most of the smaller tools.
  * Standards-Version 4.1.3, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 09 Jan 2018 23:15:00 +0100

scummvm-tools (1.9.0-2) unstable; urgency=medium

  * Avoid disabling -Wformat, which causes a build failure with GCC 7.
    Closes: #871066.
  * Update debian/copyright.
  * Standards-Version 4.0.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 09 Aug 2017 22:36:33 +0200

scummvm-tools (1.9.0-1) unstable; urgency=medium

  * New upstream release, including compilation-fixes.patch.
  * Add spelling-fixes.patch to fix a lintian-detected spelling mistake.
  * Standards-Version 3.9.8, no change required.
  * Switch to debhelper compatibility level 10.

 -- Stephen Kitt <skitt@debian.org>  Tue, 15 Nov 2016 13:26:03 +0100

scummvm-tools (1.8.0-2) unstable; urgency=medium

  [ Phil Morrell ]
  * Fix debian/watch to exclude -win32.zip but still allow release
    candidates.

  [ Stephen Kitt ]
  * Allow building on kFreeBSD and Hurd. Closes: #814753.
  * Clean up debian/control with cme.
  * Standards-Version 3.9.7, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 28 Mar 2016 22:51:03 +0200

scummvm-tools (1.8.0-1) unstable; urgency=medium

  * New upstream release, including spelling-fixes.patch.

 -- Stephen Kitt <skitt@debian.org>  Fri, 18 Mar 2016 13:40:38 +0100

scummvm-tools (1.7.0-1) unstable; urgency=low

  * Initial release. Closes: #653617.

 -- Stephen Kitt <skitt@debian.org>  Sun, 07 Feb 2016 23:20:38 +0100
